from tkinter import *
from collections import defaultdict
from tkinter.messagebox import showinfo
import random

large_font = ('Verdana',40)
medium_font = ('Verdana', 20)
small_font = ('Verdana', 10)
button_font = ('Serif', 14)

class Game:
    def __init__(self, filepath, window):
        self.questions = {}
        self.score = 0
        self.current_level = 0
        self.current_correct_answer = ''
        self.window = window
        self.levels_scores = {
            1 : 100,
            2 : 200,
            3 : 300,
            4 : 400,
            5 : 500,
            6 : 1000,
            7 : 2500,
            8 : 5000,
            9 : 10000,
            10 : 25000,
            11 : 50000,
            12 : 100000,
            13 : 250000,
            14 : 500000,
            15 : 1000000
        }

        # Citim fisierul cu intrebari
        with open(filepath, encoding='utf8') as file:
            for line in file:
                if line == '\n':
                    continue

                question_row = line.strip()

                '''
                question_row = 1000000 Care este durata de viaTa a unei libelule? -> 24 ore,pina o prinzi,30 de zile, un an
                '''
                question_data = question_row.split(" ",1)

                # amenajam intrebarile dupa scor
                if (question_data[0] not in self.questions):
                    self.questions[question_data[0]] = []

                question_answer = question_data[1].split("->")

                answers = question_answer[1].split(",")

                self.questions[question_data[0]].append({'score': question_data[0],'question': question_answer[0].strip(), 'answers': answers})

        file.close()

    def begin(self, score = 0):

        self.score = score

        self.current_level +=1
        
        self.entry_list = random.choice(self.questions[str(self.levels_scores[self.current_level])])

        print(self.entry_list) # {'score': '400', 'question': 'Care este moneda oficiala a Bulgariei?', 'answers': [' Leva', 'Lira', 'Dolarul', 'Euro']}
        
        # Rescriem label-ul cu numarul intrebarii
        question_label_title.config(text = f"Intrebarea numarul: {self.current_level}") 
        
        question_label.config(text = self.entry_list['question'])
        
        score_label.config(text=f"Score: {self.score}")

        self.current_correct_answer = (self.entry_list['answers'][0]).strip()

        random_answers = [0,1,2,3]

        random.shuffle(random_answers)

        button_1.config(text = (self.entry_list['answers'][random_answers[0]]).strip())
        button_2.config(text = (self.entry_list['answers'][random_answers[1]]).strip())
        button_3.config(text = (self.entry_list['answers'][random_answers[2]]).strip())
        button_4.config(text = (self.entry_list['answers'][random_answers[3]]).strip())

        #self.current_correct_answer = entry_list['answers'][0]


    def question1(self):
        if self.current_correct_answer == button_1.cget('text'):
            self.you_win() if self.current_level == 15 else self.begin(self.entry_list['score'])
        else:
            self.game_over()
    
    def question2(self):
        if self.current_correct_answer == button_2.cget('text'):
            self.you_win() if self.current_level == 15 else self.begin(self.entry_list['score'])
        else:
            self.game_over()
    
    def question3(self):
        if self.current_correct_answer == button_3.cget('text'):
            self.you_win() if self.current_level == 15 else self.begin(self.entry_list['score'])
        else:
            self.game_over()

    def question4(self):
        if self.current_correct_answer == button_4.cget('text'):
            self.you_win() if self.current_level == 15 else self.begin(self.entry_list['score'])
        else:
            self.game_over()

    def game_over(self):
        showinfo("",f"Raspuns gresit ! \nCistigul d-stra este: {self.score}")
        self.window.destroy()
    
    def you_win(self):
        score_label.config(text=f"Score: 1.000.000 !!!!")
        showinfo("Felicitari!",f"Felicitari !!!!\nAti cistigat milionul!!!!")
        self.window.destroy()


# inceput de program
window = Tk()
window.title("Cine vrea sa devina milionar?")
window.geometry("800x600")
game = Game("questions.txt",window)

body_frame = Frame(window,width=800, height=600,background="#ccc000" )
body_frame.pack()

# Title
Label(body_frame, text="Vrei sa devii milionar?", background="#cccccc", font=medium_font).place(x=0,y=0,width=800, height = 60)

# Intrebarea numarul #
question_label_title = Label(body_frame, text="Intrebarea numarul: ", background="#ccc000", font=medium_font)
question_label_title.place(x=0,y=60,width=800, height = 60)

# intrebarea
question_label = Label(body_frame, text="",background="#ccc000", font=small_font) 
question_label.place(x=0,y=120,width=800, height = 60)

# Intrebarea 1
button_1 = Button(window, text="Add Entry", command=game.question1, font=button_font)
button_1.place(x=0,y=180,width=400, height = 50)

# Intrebarea 2
button_2 = Button(window, text="Update Selected", command=game.question2, font=button_font )
button_2.place(x=400,y=180,width=400, height = 50)

# Intrebarea 3
button_3 = Button(window, text="Delete Selected", command=game.question3, font=button_font)
button_3.place(x=0,y=230,width=400, height = 50)

# Intrebarea 4
button_4 = Button(window, text="Close", command=game.question4, font=button_font)
button_4.place(x=400,y=230,width=400, height = 50)

# Score:
score_label = Label(body_frame, text="Score: ",background="#ccc000", font=medium_font) #anchor Est se aliniaza la dreapta (anchor=E)
score_label.place(x=0,y=280,width=800, height = 60)

game.begin()
# sf. de program
window.mainloop()

    
